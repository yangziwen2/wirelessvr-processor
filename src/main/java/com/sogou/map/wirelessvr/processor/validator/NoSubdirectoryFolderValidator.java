package com.sogou.map.wirelessvr.processor.validator;

import java.io.File;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.ParameterException;


public class NoSubdirectoryFolderValidator implements IValueValidator<File> {

	@Override
	public void validate(String name, File folder) throws ParameterException {
		if(folder == null || !folder.isDirectory()) {
			throw new ParameterException("Target folder is not valid!");
		}
		File[] files = folder.listFiles();
		if(files == null || files.length == 0) {
			throw new ParameterException("There is no file inside the target folder!");
		}
		for(File file: files) {
			if(file != null && file.isDirectory()) {
				throw new ParameterException("There is subdirectory inside the folder!");
			}
		}
	}

}
