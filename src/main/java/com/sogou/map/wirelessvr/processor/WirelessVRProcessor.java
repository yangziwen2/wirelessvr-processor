package com.sogou.map.wirelessvr.processor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.commons.io.IOUtils;

public class WirelessVRProcessor {
	
	public static void process(File src, File target, String originUrlType, String urlPrefix) {
		BufferedReader reader = null;
		BufferedWriter writer = null;
		if(target.exists()) {
			target.delete();
		}
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(src), "gbk"));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(target), "gbk"));
			String line = "";
			while((line = reader.readLine()) != null) {
				writer.write(processLine(line, originUrlType, urlPrefix));
				writer.write("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(reader);
			IOUtils.closeQuietly(writer);
			System.out.println(String.format("Add [%s] to [%s]!", originUrlType, target.getAbsolutePath()));
		}
		
	}
	
	private static String processLine(String line, String originUrlType, String urlPrefix) {
		if(line.indexOf("http://map.sogou.com/?ad=1&city=") > 0) {
			return line.replace("http://map.sogou.com/?ad=1&city=", urlPrefix + "?ad=1&originurltype=" + originUrlType + "&city=");
		}
		if(line.indexOf("http://map.sogou.com/?ad=1&amp;city") > 0) {
			return line.replace("http://map.sogou.com/?ad=1&amp;city", urlPrefix + "?ad=1&amp;originurltype=" + originUrlType + "&amp;city");
		}
		if(line.indexOf("http://map.sogou.com/?ad=1&c=") > 0) {
			return line.replace("http://map.sogou.com/?ad=1&c=", urlPrefix + "?ad=1&originurltype=" + originUrlType + "&c=");
		}
		if(line.indexOf("http://map.sogou.com/?ad=1&amp;c=") > 0) {
			return line.replace("http://map.sogou.com/?ad=1&amp;c=", urlPrefix + "?ad=1&amp;originurltype=" + originUrlType + "&amp;c=");
		}
		if(line.indexOf("http://map.sogou.com/#from=RoadVR&amp;") > 0) {
			return line.replace("http://map.sogou.com/#from=RoadVR&amp;", urlPrefix + "#from=RoadVR&amp;originurltype=" + originUrlType +"&amp;");
		}
		if(line.indexOf("http://map.sogou.com/#from=RoadVR&") > 0) {
			return line.replace("http://map.sogou.com/#from=RoadVR&", urlPrefix + "#from=RoadVR&originurltype=" + originUrlType +"&");
		}
		return line;
	}
}
