package com.sogou.map.wirelessvr.processor;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class Main {

	public static void main(String[] args) {
		if(ArrayUtils.isEmpty(args)) {
			args = new String[]{"--help"};
		}
		Command command = new Command();
		JCommander commander = null;
		try {
			commander = new JCommander(command, args);
		} catch (ParameterException e) {
			System.err.println(e.getMessage());
			return;
		}
		if(command.help) {
			System.out.println("This tool is used to add stats flag to links inside wirelessVR xml data files");
			commander.usage();
			return;
		}
		try {
			processXmlInsideFolder(command.targetFolder, command.originUrlType, command.urlPrefix);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("The replacement job is done!");
	}
	
	private static void processXmlInsideFolder(File folder, String originUrlType, String urlPrefix) throws IOException {
		// 备份folder
		String backupSuffix = "_bak_" + DateFormatUtils.format(new Date(), "yyyyMMdd_HHmmss");
		String folderPath = folder.getAbsolutePath();
		File backupFolder = new File(folderPath + backupSuffix);
		FileUtils.copyDirectory(folder, backupFolder);
		for(File file: folder.listFiles()) {
			if(!file.isFile()) {
				continue;
			}
			if(!FilenameUtils.isExtension(file.getName(), "xml")) {
				continue;
			}
			File srcFile = new File(FilenameUtils.concat(backupFolder.getAbsolutePath(), file.getName()));
			File targetFile = file;
			WirelessVRProcessor.process(srcFile, targetFile, originUrlType, urlPrefix);
		}
		
		
	}
	
}
