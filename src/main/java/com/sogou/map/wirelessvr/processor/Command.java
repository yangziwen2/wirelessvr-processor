package com.sogou.map.wirelessvr.processor;

import java.io.File;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.sogou.map.wirelessvr.processor.validator.NoSubdirectoryFolderValidator;

@Parameters(
	separators = "=", 
	commandDescription = "Used to add stats flag to wireless VR xml data files"
)
public class Command {

	@Parameter(
		names = {"-f", "--target-folder"},
		description = "the parent folder of the xml files",
		validateValueWith = NoSubdirectoryFolderValidator.class,
		required = true
	)
	File targetFolder;
	
	@Parameter(
		names = {"-t", "--origin-url-type"},
		description = "the type of data to be processed(VR_Highway, VR_Province, VR_City, VR_District)"
	)
	String originUrlType = "VR_Highway";
	
	@Parameter(
		names = {"-p", "--url-prefix"},
		description = "the prefix of the whole link url, can be set to test env url if necessary"
	)
	String urlPrefix = "http://map.sogou.com/";
	
	@Parameter(names = {"-h", "--help"}, description = "print this message", help = true)
	Boolean help = false;
	
}
